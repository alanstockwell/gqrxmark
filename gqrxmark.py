import re
import csv


class Tag:
    _color_regex = re.compile(r'^#(?:[0-9a-fA-F]{3}){1,2}$')

    def __init__(self, name, color='#c0c0c0'):
        if not self._color_regex.match(color):
            raise ValueError('Invalid color: {}'.format(color))

        self.name = name
        self.color = color

    def __str__(self):
        return '{}; {}'.format(self.name, self.color)

    def __repr__(self):
        return '<Tag: {}>'.format(self)

    def as_row(self):
        return (
            self.name.ljust(20),
            ' {}'.format(self.color),
        )


class Channel:
    MOD_DEMOD_OFF = 'Demod Off'
    MOD_RAW_I_Q = 'Raw I/Q'
    MOD_AM = 'AM'
    MOD_AM_SNC = 'AM-Sync'
    MOD_LSB = 'LSB'
    MOD_USB = 'USB'
    MOD_CW_L = 'CW-L'
    MOD_CW_U = 'CW-U'
    MOD_NARROW_FM = 'Narrow FM'
    MOD_WFM_MONO = 'WFM (mono)'
    MOD_WFM_STEREO = 'WFM (stereo)'
    MOD_WFM_OIRT = 'WFM (oirt)'

    MODULATIONS = (
        MOD_DEMOD_OFF,
        MOD_RAW_I_Q,
        MOD_AM,
        MOD_AM_SNC,
        MOD_LSB,
        MOD_USB,
        MOD_CW_L,
        MOD_CW_U,
        MOD_NARROW_FM,
        MOD_WFM_MONO,
        MOD_WFM_STEREO,
        MOD_WFM_OIRT,
    )

    def __init__(self, frequency, name, modulation, bandwidth, tags=None):
        if modulation not in self.MODULATIONS:
            raise ValueError('Invalid modulation: {}'.format(modulation))

        self.frequency = int(frequency)
        self.name = name
        self.modulation = modulation
        self.bandwidth = int(bandwidth)

        if tags is None:
            self.tags = []
        elif isinstance(tags, str):
            self.tags = [tags]
        else:
            self.tags = list(tags)

    def __str__(self):
        return '; '.join((str(_) for _ in (
            self.frequency,
            self.name,
            self.modulation,
            self.bandwidth,
            self.tags,
        )))

    def __repr__(self):
        return '<Channel: {}>'.format(self)

    def as_row(self):
        return (
            str(self.frequency).rjust(12),
            ' {}'.format(self.name).ljust(26),
            ' {}'.format(self.modulation).ljust(21),
            ' {}'.format(self.bandwidth).rjust(11),
            ' {}'.format(','.join(self.tags)),
        )


class Bookmarks:
    file_path = None

    def __init__(self, file_path=None):
        if file_path is None:
            self.tags = []
            self.channels = []
        else:
            self.load(file_path)

    def reload(self):
        if self.file_path is None:
            raise ValueError('No file path set')

        self.load(self.file_path)

    def load(self, file_path):
        self.file_path = file_path

        self.tags = []
        self.channels = []

        in_tags = False
        in_channels = False

        for row in csv.reader(open(self.file_path), delimiter=';'):
            try:
                if not in_tags and row[0].startswith('# Tag name'):
                    in_tags = True

                    continue

                if not in_channels and row[0].startswith('# Frequency'):
                    in_channels = True

                    continue
            except IndexError:
                in_tags = False
                in_channels = False

                continue

            if in_tags:
                self.tags.append(Tag(*(_.strip() for _ in row)))
            elif in_channels:
                self.channels.append(Channel(*(_.strip() for _ in row)))

    def save(self, file_path=None):
        if self.file_path is None and file_path is None:
            raise ValueError('No file path specified')
        elif file_path is not None:
            self.file_path = file_path

        with open(self.file_path, 'w') as out_file:
            cw = csv.writer(out_file, delimiter=';')

            cw.writerow((
                '# Tag name          ',
                '  color',
            ))

            for tag in self.tags:
                cw.writerow(tag.as_row())

            cw.writerow(())
            cw.writerow((
                '# Frequency ',
                ' Name                     ',
                ' Modulation          ',
                '  Bandwidth',
                ' Tags',
            ))

            for channel in self.channels:
                cw.writerow(channel.as_row())
